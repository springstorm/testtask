package com.testtask;

import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.testtask.pages.HomePage;

import java.util.concurrent.TimeUnit;

public class SampleTestNgTest extends TestNgTestBase {

  private HomePage homepage;

  @BeforeMethod
  public void initPageObjects() {
    homepage = PageFactory.initElements(driver, HomePage.class);
  }

  @Test
  public void testHomePageHasAHeader() {

      driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
      driver.get(baseUrl);
      driver.findElement(By.id("search_from_str")).sendKeys("automated testing");
      driver.findElement(By.name("search")).click();
      int i = driver.findElements(By.className("b-results__li")).size();
      System.out.print(i);
      boolean b;
      if (driver.getPageSource().contains("Minsk Automated Testing Community")) b = true;
      else b = false;
      System.out.print(b);


      Assert.assertFalse("".equals(homepage.header.getText()));
  }
}
