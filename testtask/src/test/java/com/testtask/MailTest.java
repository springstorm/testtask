package com.testtask;

import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.testtask.pages.HomePage;

import java.util.concurrent.TimeUnit;

public class MailTest extends TestNgTestBase {

    private HomePage homepage;

    @BeforeMethod
    public void initPageObjects() {
        homepage = PageFactory.initElements(driver, HomePage.class);
    }

    @Test
    public void testHomePageHasAHeader() {

        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.get(baseUrl);
        driver.findElement(By.id("Email")).sendKeys("Springstorm666@gmail.com");
        driver.findElement(By.id("next")).click();
        driver.findElement(By.id("Passwd")).sendKeys("6279508Rino5");
        driver.findElement(By.id("signIn")).click();
        driver.findElement(By.partialLinkText("Входящие")).click();
        driver.findElement(By.partialLinkText("Отправленные")).click();
//        driver.findElement(By.partialLinkText("Свернуть ярлык \"Категории\"")).click();
  //      driver.findElement(By.partialLinkText("Свернуть ярлык \"Круги\"")).click();
    //    driver.findElement(By.partialLinkText("Спам")).click();
        driver.findElement(By.partialLinkText("Входящие")).click();
        driver.findElement(By.className("aoq")).click();
        driver.findElement(By.xpath("/html/body/div[19]/div/div[2]/div[1]/span[2]")).click();
        driver.findElement(By.partialLinkText("Входящие")).click();
        driver.findElement(By.id(":pk")).sendKeys("а");

        Assert.assertFalse("".equals(homepage.header.getText()));
    }}